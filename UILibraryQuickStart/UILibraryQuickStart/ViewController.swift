import UIKit
import AzureCommunicationCalling
import AzureCommunicationUI
import AVFoundation

class ViewController: UIViewController {
    
 var meetingLink: String = ""
     var callee: String = "ravi"

     var callStatus: String = ""
     var message: String = ""
     var recordingStatus: String = ""
     var callClient: CallClient?
     var callAgent: CallAgent?
     var call: Call?
//     var callObserver: CallObserver?
var url = "https://teams.microsoft.com/l/meetup-join/19%3ameeting_M2IzYzczNTItYmY3OC00MDlmLWJjMzUtYmFiMjNlOTY4MGEz%40thread.skype/0?context=%7b%22Tid%22%3a%2272f988bf-86f1-41af-91ab-2d7cd011db47%22%2c%22Oid%22%3a%22550fae72-d251-43ec-868c-373732c2704f%22%7d"
    var token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjEwMyIsIng1dCI6Ikc5WVVVTFMwdlpLQTJUNjFGM1dzYWdCdmFMbyIsInR5cCI6IkpXVCJ9.eyJza3lwZWlkIjoiYWNzOjA5YWY3N2Y5LTk4NzEtNGRmMy1iMDg4LTM2Zjc1NGQxODFkZl8wMDAwMDAwZS1mMWUxLTgxMmQtMzVmMy0zNDNhMGQwMGE1YzkiLCJzY3AiOjE3OTIsImNzaSI6IjE2NDIwNTMyMjkiLCJleHAiOjE2NDIxMzk2MjksImFjc1Njb3BlIjoiY2hhdCx2b2lwIiwicmVzb3VyY2VJZCI6IjA5YWY3N2Y5LTk4NzEtNGRmMy1iMDg4LTM2Zjc1NGQxODFkZiIsImlhdCI6MTY0MjA1MzIyOX0.rYquSnJFNI-d_h1j7Cf42ebfWeDmBIGYjl6vtwSIx-ypwnyhrUQVx7EgBchn0cWIHl4El_V2cyVYeA8bE3pFV46cH4T4oq5fpLqTVlOWoOLy9E-lpFWrGUIcYVxjaX7u7-GvHLmafLqFX7eZY8NXS2X1XTiu2Q12pjWpajXeUJslz5dNMPO_0YoYTrR3ECs4RlfJ93ogZAElxmpDlyoVr9wq5JIYkhRSZgekGnRImd48SV7KZi2f__4WIT0th52RiIKqedjh7mz7qRZcf7PmxzqujoPC5eJaLnielJXNSGbcOphujq0fj1oIWj3W1BL719im6qlXOha-J1duS1-6yQ"
    var groupId = "8e3d00e5-8ec3-4a4d-b5d2-3b7a4fad9fb6"
    
    private var callComposite: CallComposite?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    func initialCall(){
        // Initialize call agent
        var userCredential: CommunicationTokenCredential?
        do {
            userCredential = try CommunicationTokenCredential(token: token)
        } catch {
            print("ERROR: It was not possible to create user credential.")
//            self.message = "Please enter your token in source code"
            return
        }
        
        self.callClient = CallClient()
        
        // Creates the call agent
        self.callClient?.createCallAgent(userCredential: userCredential!) { (agent, error) in
            if error != nil {
                self.message = "Failed to create CallAgent."
                return
            } else {
                self.callAgent = agent
                self.message = "Call agent successfully created."
            }
        }
    }
    
    @IBAction func didTapstartCallComposite(_ sender: UIButton) {
        
//        startCall();
//        return
        let callCompositeOptions = CallCompositeOptions(themeConfiguration: CustomThemeConfiguration())
        
        callComposite = CallComposite(withOptions: callCompositeOptions)
        
        let communicationTokenCredential = try! CommunicationTokenCredential(token: token)
        
//        let options = GroupCallOptions(communicationTokenCredential: communicationTokenCredential,
//                                       groupId: UUID(uuidString: groupId)!,
//                                       displayName: "Group call")
        
        let options = TeamsMeetingOptions(communicationTokenCredential: communicationTokenCredential,
                                          meetingLink: url,
                                          displayName: "Teams MeetingOptions")

//        let options = TeamsMeetingOptions(communicationTokenCredential: communicationTokenCredential,
//                                          meetingLink: url,
//                                          displayName: "Ramesh kumar s")

        callComposite?.setTarget(didFail: { error in
            print("didFail with error:\(error)")
        })
        callComposite?.launch(with: options)
        
    }
    
    @IBAction func didTapJoinMeating(_ sender: UIButton) {
        
        let callCompositeOptions = CallCompositeOptions(themeConfiguration: CustomThemeConfiguration())
        
        callComposite = CallComposite(withOptions: callCompositeOptions)
        
        let communicationTokenCredential = try! CommunicationTokenCredential(token: token)
        
//        let options = GroupCallOptions(communicationTokenCredential: communicationTokenCredential,
//                                       groupId: UUID(uuidString: groupId)!,
//                                       displayName: "Ramesh kumar")
        
        let options = TeamsMeetingOptions(communicationTokenCredential: communicationTokenCredential,
                                          meetingLink: url,
                                          displayName: "Teams MeetingOptions")

        callComposite?.setTarget(didFail: { error in
            print("didFail with error:\(error)")
        })
        callComposite?.launch(with: options)

        
    }
    
    @IBAction func didTapLeaveMeeting(_ sender: Any) {
        var userCredential: CommunicationTokenCredential?
        do {
            userCredential = try CommunicationTokenCredential(token: token)
        } catch {
            print("ERROR: It was not possible to create user credential.")
            return
        }

        self.callClient = CallClient()

        // Creates the call agent
        self.callClient?.createCallAgent(userCredential: userCredential!) { (agent, error) in
            if error != nil {
                print("ERROR: It was not possible to create a call agent.")
                return
            }
            else {
                self.callAgent = agent
                print("Call agent successfully created.")
                self.startCall()

            }
        }


    }
    
    func startCall()
    {
        // Ask permissions
        AVAudioSession.sharedInstance().requestRecordPermission { (granted) in
            if granted {
                // start call logic
                let callees:[CommunicationIdentifier] = [CommunicationUserIdentifier(self.callee)]
                self.callAgent?.startCall(participants: callees, options: StartCallOptions()) { (call, error) in
                    if (error == nil) {
                        self.call = call
                    } else {
                        print("Failed to get call object")
                    }
                }
            }
        }
    }

}

class CustomThemeConfiguration: ThemeConfiguration {
//    var primaryColor: UIColor {
//        return UIColor.green
//    }
}


//class CallObserver : NSObject, CallDelegate {
//
//    public func call(_ call: Call, didChangeState args: PropertyChangedEventArgs) {
//        owner.callStatus = CallObserver.callStateToString(state: call.state)
//        if call.state == .disconnected {
//            owner.call = nil
//            owner.message = "Left Meeting"
//        } else if call.state == .inLobby {
//            owner.message = "Waiting in lobby !!"
//        } else if call.state == .connected {
//            owner.message = "Joined Meeting !!"
//        }
//    }
//
//    public func call(_ call: Call, didChangeRecordingState args: PropertyChangedEventArgs) {
//        if (call.isRecordingActive == true) {
//            owner.recordingStatus = "This call is being recorded"
//        }
//        else {
//            owner.recordingStatus = ""
//        }
//    }
//
//    private static func callStateToString(state: CallState) -> String {
//        switch state {
//        case .connected: return "Connected"
//        case .connecting: return "Connecting"
//        case .disconnected: return "Disconnected"
//        case .disconnecting: return "Disconnecting"
//        case .earlyMedia: return "EarlyMedia"
//        case .none: return "None"
//        case .ringing: return "Ringing"
//        case .inLobby: return "InLobby"
//        default: return "Unknown"
//        }
//    }
//}
//
//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}

# MS Teams - Azure Communication UI Library for iOS
* This project is for demoing MS Teams - Azure Communication UI Library SDK in iOS. 
* The UI library will render a full communication experience right into your application.  
* It takes care of connecting to the desired call and setting it up behind the scenes.
* As a developer you just need to worry about where in your experience you want the communication experience to launch.
* The composite takes the user through setting up their devices, joining the call and participating in it, and rendering other participants.



### MS Teams SDK features

* Schedule Meeting
* Instant Meeting
* Meeting Invite
* Guest user
* Screen Sharing
* Raising hand

## Getting Started

Get started with Azure Communication Services by using the UI Library to quickly integrate communication experiences into your applications. In this quickstart, you'll learn how to integrate the composites into your applications.



### Prerequisites

* An Azure account with an active subscription. [Create an account for free](https://azure.microsoft.com/free/?WT.mc_id=A261C142F).
* A Mac running [Xcode](https://go.microsoft.com/fwLink/p/?LinkID=266532), along with a valid developer certificate installed into your Keychain. [CocoaPods](https://cocoapods.org/) must also be installed to fetch dependencies.
* A deployed Communication Services resource. Create a [Communication Services resource](https://docs.microsoft.com/azure/communication-services/quickstarts/create-communication-resource).
* Azure Communication Services Token. [See example](https://docs.microsoft.com/en-us/azure/communication-services/quickstarts/identity/quick-create-identity).


#### Requirements

* iOS 13+
* Xcode 11+
* Swift 5.0+

#### Using CocoaPods

CocoaPods is a dependency manager for Cocoa projects. To set up with CocoaPods visit their [Getting Started Guide](https://guides.cocoapods.org/using/getting-started.html). To integrate UI Mobile Library into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
pod 'AzureCommunicationUI', '1.0.0-beta.1'
```

### Quick Sample

Replace `<GROUP_CALL_ID>` with your group id for your call, `<DISPLAY_NAME>` with your name, and `<USER_ACCESS_TOKEN>` with your token.

```swift
let callCompositeOptions = CallCompositeOptions()
callComposite = CallComposite(withOptions: callCompositeOptions)
let communicationTokenCredential = try! CommunicationTokenCredential(token: "<USER_ACCESS_TOKEN>")
let options = GroupCallOptions(communicationTokenCredential: communicationTokenCredential,
                               groupId: UUID("<GROUP_CALL_ID>")!,
                               displayName: "<DISPLAY_NAME>")
callComposite?.launch(with: options)
```

For more details on Mobile UI Library functionalities visit [HERE](https://docs.microsoft.com/en-us/azure/communication-services/quickstarts/ui-library/get-started-composites?pivots=platform-ios&tabs=kotlin).

### Run the code

You can build and run your app on iOS simulator by selecting Product > Run or by using the (⌘-R) keyboard shortcut.

* Tap Start Experience.
* Accept audio permissions and select device, mic, and video settings.
* Tap Join Call.





<img src="zoom.gif">
<img src="MS_Teams.gif" width="50%">


### Feedback
If you are using this component in your project please do notify [rameshkumar.selvam@impigertech.com](mailto:rameshkumar.selvam@impigertech.com) with your project details, improvements & requirements, bugs etc.

